package simplecheckout;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import org.junit.Test;
import simplecheckout.product.fruit.Apple;
import simplecheckout.product.fruit.Orange;
import simplecheckout.util.PriceUtils;

public class CheckoutTest {
	@Test
	public void testPrices() {
		assertTrue(new BigDecimal("0.60").equals(
				new Apple().getPrice()));
		
		assertFalse(new BigDecimal("0.30").equals(
				new Orange().getPrice()));
	}
	
	@Test
	public void testTotalWithInvalidInput() {
		String[] products = new String[] {"apple", "pear", "orange"};
		
		assertTrue(new BigDecimal("0.85").equals(PriceUtils.total(products)));
	}
	
	@Test
	public void testOffers() {
		String[] products = new String[] {
			"apple", //	0.60
			"apple", // 0.00
			"apple", // 0.60
			"apple", // 0.00
			"apple", // 0.60
			"apple", // 0.00
			"apple", // 0.60
			"orange", //0.25
			"orange", //0.25
			"orange", //0.00
			"orange", //0.25
			"orange"  //0.25
		};
		
		assertTrue(new BigDecimal("3.40").equals(PriceUtils.total(products)));
	}
}
