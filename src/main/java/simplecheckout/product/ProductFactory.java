package simplecheckout.product;

import simplecheckout.product.fruit.Product;

public interface ProductFactory {
	public Product get(String type) throws InstantiationException, IllegalAccessException, ClassNotFoundException;
}
