package simplecheckout.product.fruit;

public class Apple extends Fruit {
	
	@Override
	public TYPE getType() {
		return Fruit.TYPE.apple;
	}
}
