package simplecheckout.product.fruit.factory;

import simplecheckout.product.ProductFactory;
import simplecheckout.product.fruit.Fruit;

public class FruitFactory implements ProductFactory {
	
	/**
	 * Returns a new Fruit of the specified type
	 * @param type the
	 * @return a Fruit
	 * @throws InstantiationException, IllegalAccessException, ClassNotFoundException
	 */
	@Override
	public Fruit get(String type) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		type = type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase();
		Class<?> clazz = Class.forName("simplecheckout.product.fruit." + type);
		return (Fruit) clazz.newInstance();
	}

}
