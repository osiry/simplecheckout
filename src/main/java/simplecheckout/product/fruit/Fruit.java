package simplecheckout.product.fruit;

import java.math.BigDecimal;
import java.util.ResourceBundle;

public abstract class Fruit implements Product {
	// keep this public, we are likely to reuse it elsewhere
	public enum TYPE {
		apple, orange
	}
	
	private static final ResourceBundle priceBundle;
	
	static {
		priceBundle = ResourceBundle.getBundle("price");
	}
	
	public BigDecimal getPrice() {
		
		return new BigDecimal(
				priceBundle.getString(getType().name()));
	}
	
	public abstract TYPE getType();
}
