package simplecheckout.product.fruit;
import java.math.BigDecimal;

public interface Product {
	BigDecimal getPrice();
}
