package simplecheckout.product.fruit;

public class Orange extends Fruit {
	
	@Override
	public TYPE getType() {
		return Fruit.TYPE.orange;
	}

}
