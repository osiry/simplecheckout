package simplecheckout.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import simplecheckout.product.fruit.Fruit;
import simplecheckout.product.fruit.factory.FruitFactory;

/**
 * Utility class to 
 * @author Roland
 *
 */
public class PriceUtils {
	
	private static ResourceBundle offers;
	private static FruitFactory fruitFactory = new FruitFactory();
	
	static {
		offers = ResourceBundle.getBundle("offers");
	}
	
	// TODO this method can only handle fruits as input. Should work with any Product
	/**
	 * Calculates the total and applies offers
	 * @param fruits array of fruit names
	 * @return the total
	 */
	public static BigDecimal total(String[] fruits) {
		BigDecimal total = BigDecimal.ZERO;
		
		// <product name, count>
		Map<Fruit.TYPE, Integer> counts = new HashMap<>();
		for (String fruitName : fruits) {
				Fruit fruit = null;
				try {
					fruit = fruitFactory.get(fruitName);
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e1) {
					e1.printStackTrace();
					continue;
				}
				
				int freeAt = -1;
				
				try {
					freeAt = Integer.parseInt(offers.getString(fruit.getType().name()));
				}
				catch (MissingResourceException e) {}
				
				if (!counts.containsKey(fruit.getType())) {
					counts.put(fruit.getType(), 1);
				}
				else {
					counts.put(fruit.getType(), counts.get(fruit.getType()) + 1);
				}
				
				if (freeAt != -1 && counts.get(fruit.getType()) == freeAt) {
					counts.put(fruit.getType(), 0);
				}
				else {
					total = total.add(fruit.getPrice());
				}
		}
		
		return total.setScale(
				2, RoundingMode.HALF_UP);
	}
}
